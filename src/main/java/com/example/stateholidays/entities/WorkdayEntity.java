package com.example.stateholidays.entities;


import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "workdays")
public class WorkdayEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_of")
    private LocalDate date;

    private String description;


    public WorkdayEntity() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
