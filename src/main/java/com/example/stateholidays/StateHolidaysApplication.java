package com.example.stateholidays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StateHolidaysApplication {

    public static void main(String[] args) {
        SpringApplication.run(StateHolidaysApplication.class, args);
    }

}
