package com.example.stateholidays.servicies;


import com.example.stateholidays.dto.Day;
import com.example.stateholidays.dto.PeriodInfoDTO;
import com.example.stateholidays.dto.PeriodInfoRequest;
import com.example.stateholidays.entities.HolidayEntity;
import com.example.stateholidays.entities.WorkdayEntity;
import com.example.stateholidays.repositries.HolidayRepository;
import com.example.stateholidays.repositries.WorkdayRepository;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class MainService {

    private final HolidayRepository holidayRepository;

    private final WorkdayRepository workdayRepository;

    public MainService(HolidayRepository holidayRepository, WorkdayRepository workdayRepository) {
        this.holidayRepository = holidayRepository;
        this.workdayRepository = workdayRepository;
    }

    public PeriodInfoDTO holidaysInPeriod(PeriodInfoRequest request) {

        LocalDate dateFrom = request.getDateFrom();
        LocalDate dateTo = request.getDateTo().plusDays(1);

        // a - количество дней в периоде
        // b - количество уикэндов
        // c - количество праздников
        // d - количество рабочих дней перенесенных на выходные
        // hol - b+c-d
        // wor - a-hol

        long a = DAYS.between(dateFrom, dateTo);

        long b = getWeekends(dateFrom, dateTo);
        long c = getHolidays(dateFrom, dateTo);
        long d = getWorkdays(dateFrom, dateTo);

        long holidays = b + c - d;
        long workdays = a - holidays;

        return new PeriodInfoDTO(workdays, holidays);
    }

    private long getWorkdays(LocalDate dateFrom, LocalDate dateTo) {
        return workdayRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(dateFrom, dateTo)
                .size();
    }

    private long getHolidays(LocalDate dateFrom, LocalDate dateTo) {
        return holidayRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(dateFrom, dateTo)
                .size();
    }

    private long getWeekends(LocalDate dateFrom, LocalDate dateTo) {
        return dateFrom.datesUntil(dateTo)
                .filter(it -> isWeekend(it))
                .count();
    }

    public List<Day> listInPeriod(PeriodInfoRequest request) {

        LocalDate dateFrom = request.getDateFrom();
        LocalDate dateTo = request.getDateTo().plusDays(1);

        List<WorkdayEntity> workdayEntityList = workdayRepository.findAll();
        List<HolidayEntity> holidayEntityList = holidayRepository.findAll();
        List<Day> days = dateFrom.datesUntil(dateTo)
                .map(localDate -> {
                    boolean isHoliday = (
                            isWeekend(localDate)
                                    && isNotWorkingDay(localDate, workdayEntityList)
                    ) ||
                            isHoliday(localDate, holidayEntityList);
                    return new Day(localDate, isHoliday);
                }).collect(Collectors.toList());

        return days;
    }


    private boolean isWeekend(LocalDate date) {
        return date.getDayOfWeek() == DayOfWeek.SATURDAY ||
                date.getDayOfWeek() == DayOfWeek.SUNDAY;
    }

    private boolean isHoliday(LocalDate date, List<HolidayEntity> holidayEntityList) {
        return holidayEntityList.stream()
                .anyMatch(it -> it.getDate().isEqual(date));
    }

    private boolean isNotWorkingDay(LocalDate date, List<WorkdayEntity> workdayEntityList) {
        return workdayEntityList.stream()
                .noneMatch(it -> it.getDate().isEqual(date));
    }

}
