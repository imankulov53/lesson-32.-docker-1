package com.example.stateholidays.dto;

public class MonthInfoRequest {

    private Short month;
    private Short year;

    public MonthInfoRequest(Short month, Short year) {
        this.month = month;
        this.year = year;
    }

    public Short getMonth() {
        return month;
    }

    public void setMonth(Short month) {
        this.month = month;
    }

    public Short getYear() {
        return year;
    }

    public void setYear(Short year) {
        this.year = year;
    }
}
