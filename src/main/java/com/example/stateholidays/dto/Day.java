package com.example.stateholidays.dto;

import java.time.LocalDate;

public class Day {

    private LocalDate date;
    private boolean isHoliday;


    public Day(LocalDate date, boolean isHoliday) {
        this.date = date;
        this.isHoliday = isHoliday;
    }


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isHoliday() {
        return isHoliday;
    }

    public void setHoliday(boolean holiday) {
        isHoliday = holiday;
    }
}
