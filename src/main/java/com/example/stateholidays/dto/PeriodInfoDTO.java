package com.example.stateholidays.dto;

public class PeriodInfoDTO {

    private Long workdays;
    private Long holidays;


    public PeriodInfoDTO(Long workdays, Long holidays) {
        this.workdays = workdays;
        this.holidays = holidays;
    }


    public Long getWorkdays() {
        return workdays;
    }

    public void setWorkdays(Long workdays) {
        this.workdays = workdays;
    }

    public Long getHolidays() {
        return holidays;
    }

    public void setHolidays(Long holidays) {
        this.holidays = holidays;
    }
}
