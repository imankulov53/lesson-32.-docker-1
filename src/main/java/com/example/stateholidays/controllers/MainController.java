package com.example.stateholidays.controllers;

import com.example.stateholidays.dto.Day;
import com.example.stateholidays.dto.MonthInfoRequest;
import com.example.stateholidays.dto.PeriodInfoDTO;
import com.example.stateholidays.dto.PeriodInfoRequest;
import com.example.stateholidays.servicies.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.List;


@RestController
@RequestMapping("/api/info")
public class MainController {

    @Autowired
    private MainService service;

    @PostMapping("/month")
    public PeriodInfoDTO holidaysInMonth(@RequestBody MonthInfoRequest request) {
        return service.holidaysInPeriod(toPeriodInfo(request));
    }

    @PostMapping("/period")
    public PeriodInfoDTO holidaysInPeriod(@RequestBody PeriodInfoRequest request) {
        return service.holidaysInPeriod(request);
    }

    @PostMapping("/month/list")
    public List<Day> listInMonth(@RequestBody MonthInfoRequest request) {
        return service.listInPeriod(toPeriodInfo(request));
    }

    @PostMapping("/period/list")
    public List<Day> periodInMonth(@RequestBody PeriodInfoRequest request) {
        return service.listInPeriod(request);
    }

    private PeriodInfoRequest toPeriodInfo(MonthInfoRequest request) {
        Short month = request.getMonth();
        Short year = request.getYear();

        Year year1 = Year.of(year);
        Month month1 = Month.of(month);
        int length = month1.length(year1.isLeap());

        LocalDate dateFrom = LocalDate.of(year, month1, 1);
        LocalDate dateTo = LocalDate.of(year, month1, length);

        return new PeriodInfoRequest(dateFrom, dateTo);
    }
}
