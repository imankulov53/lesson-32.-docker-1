
-- holidays
insert into holidays(date_of, description) values ('2021-01-01', 'Новый Год 1');
insert into holidays(date_of, description) values ('2021-01-04', 'Новы Год 2 (с субботы 2 января переносится на 4 января)');
insert into holidays(date_of, description) values ('2021-01-07', 'Православное Рождество');
insert into holidays(date_of, description) values ('2021-03-08', 'Международный женский день');
insert into holidays(date_of, description) values ('2021-03-22', 'Наурыз мейрамы 1');
insert into holidays(date_of, description) values ('2021-03-23', 'Наурыз мейрамы 2');
insert into holidays(date_of, description) values ('2021-03-24', 'Наурыз мейрамы 3 (21 марта воскресенье переносится на 24 марта)');
insert into holidays(date_of, description) values ('2021-05-03', 'Праздник единства народов Казахстана (Перенесен с субботы 1 мая на 3 мая)');
insert into holidays(date_of, description) values ('2021-05-07', 'День защитника Отечества');
insert into holidays(date_of, description) values ('2021-05-10', 'День Победы (перенесен с воскресенья 9 мая на 10 мая )');
insert into holidays(date_of, description) values ('2021-07-05', 'Выходной перенесенный с субботы 3 июля');
insert into holidays(date_of, description) values ('2021-07-06', 'День столицы');
insert into holidays(date_of, description) values ('2021-07-20', 'Первый день Курбан-айта');
insert into holidays(date_of, description) values ('2021-08-30', 'День Конституции РК');
insert into holidays(date_of, description) values ('2021-12-01', 'День Первого Президента РК');
insert into holidays(date_of, description) values ('2021-12-16', 'День Независимости 1');
insert into holidays(date_of, description) values ('2021-12-17', 'День Независимости 2');

--workdays
insert into workdays(date_of, description) values ('2021-07-03', 'Выходной суббота 3 июля перенесенный на 5 июля ');