create table if not exists holidays
(
    id          bigint auto_increment primary key,
    date_of     date not null,
    description varchar
);

create unique index if not exists holidays_date_uindex
    on holidays (DATE_OF desc);


create table if not exists workdays
(
    id          bigint auto_increment primary key,
    date_of     date not null,
    description varchar
);

create unique index if not exists workdays_date_uindex
    on workdays (DATE_OF desc);

