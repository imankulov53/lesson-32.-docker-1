FROM gradle:jdk11 as builder
COPY --chown=gradle:gradle . .
RUN gradle build

FROM openjdk:11.0.10-jre-slim-buster
RUN addgroup vlad && adduser --disabled-password --ingroup vlad vlad
WORKDIR /app
RUN chown vlad:vlad /app
USER vlad
COPY --from=builder --chown=vlad:vlad /home/gradle/build/libs*.jar /app/state-holidays.jar
EXPOSE 8080
ENTRYPOINT exec java -jar state-holidays.jar --spring.profiles.active=\${ACTIVE_PROFILE}